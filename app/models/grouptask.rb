class Grouptask < ActiveRecord::Base
  establish_connection :dill_db
  self.table_name = "grouptask"
  
  has_many      :usersessions, :foreign_key => "associatedgrouptaskid"
  has_many      :sessionrecordings, -> { order(associatedusersessionid: :asc) }, :through => :usersessions
  has_many      :students, :through => :usersessions
  has_one       :permission
  has_one       :lesson, :primary_key => "associatedlessonid", :foreign_key => "id"
  
  #Scopes for Task index view
  scope   :proftasks, ->(sid) { includes(:permission, :usersessions, :sessionrecordings).where("creatorstudentid = ?", sid) }
  
  scope   :studenttasks, ->(sid) { includes(:usersessions, :sessionrecordings, :students, :permission).where("student.sid = ?", sid) }
  
  #Scopes for Statistics controller
  scope :created_days_ago, ->(duration) { where('creationdate > ? AND creatorstudentid IS NOT NULL', Date.today-duration) }
  scope :sync_tasks,      ->() { where('creatorstudentid IS NOT NULL AND issynchronizedtask = 1') }
  scope :async_tasks,     ->() { where('creatorstudentid IS NOT NULL AND issynchronizedtask = 0') }
  scope :lesson_tasks,    ->() { where('creatorstudentid IS NOT NULL AND associatedlessonid >= 1') }
  scope :nonlesson_tasks, ->() { where('creatorstudentid IS NOT NULL AND associatedlessonid IS NULL') }
    
  def instructor
    return Student.where(sid: self.creatorstudentid).first
  end
    
  def student_has_recorded_in?(student)
    if self.usersessions.not_empty.where(associatedstudentsid: student.sid).count > 0
      return true
    else
      return false
    end
  end
      
end
