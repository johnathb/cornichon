class Lesson < ActiveRecord::Base
  establish_connection :dill_db
  self.table_name = "lesson"
  
  belongs_to          :grouptask
  has_one             :audio_format, :primary_key => "associatedaudioformatid", :foreign_key => "id"
  
  def filename
    return "#{self.id}.#{self.audio_format.filenameextension}"
  end
    
  def url_to_file(ssl: false)
    uri = URI.parse("#{SysParam.get(:DiLLWebServerHostURL)}dill/lessons/#{self.filename}")
    if ssl
      uri.scheme = "https"
      return uri.to_s
    else
      uri.scheme = "http"
      return uri.to_s
    end
  end
end
