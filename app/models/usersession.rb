class Usersession < ActiveRecord::Base
  establish_connection :dill_db
  self.table_name = "usersession"
  
  has_many      :sessionrecordings, -> { order(positioninlesson: :asc) },  :foreign_key => "associatedusersessionid"
  belongs_to    :student,   :foreign_key => "associatedstudentsid"
  belongs_to    :grouptask, :foreign_key => "associatedgrouptaskid"
  
  scope :not_empty, -> { joins(:sessionrecordings).where('(SELECT count(*) as total_recordings FROM sessionrecording) > 0') }

end
