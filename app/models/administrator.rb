class Administrator < ActiveRecord::Base
  establish_connection :dill_db
  self.table_name = "administrator"
  
  belongs_to :student
end
