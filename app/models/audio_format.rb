class AudioFormat < ActiveRecord::Base
  establish_connection :dill_db
  self.table_name = "audioformat"
  
  belongs_to      :lesson
end
