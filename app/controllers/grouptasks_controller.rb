class GrouptasksController < ApplicationController
  
  before_filter :protect, :only => [:index, :show]
  protect_from_forgery :only => [:index]
    
  def index
    @title = "Cornichon - Tasks List"
    @current_user_proftasks = Grouptask.proftasks(current_user.sid)
    @current_user_studenttasks = current_user.grouptasks.reject {|n| !n.student_has_recorded_in?(current_user) || !n.permission || !n.permission.isselfviewable }
  end
  
  def show    
    @task = Grouptask.includes(:usersessions, :sessionrecordings, :students).where(id: params[:id]).first
    @title = "Cornichon - #{@task.taskname}"
    @tasksessions = []
    
    # This will create the permissions if they don't exist yet, since we can't control creation of permissions along with tasks
    @permission = Permission.find_or_create_by(grouptask_id: @task.id)
    
    unless current_user.can_view_task?(@task)
      redirect_back :action => "index", :controller => "logins"
      flash[:notice] = "You're not authorized to view that task."
    end
    
    if @task.permission
      if @task.permission.isgroupviewable || @task.creatorstudentid == current_user.sid || current_user.is_super_admin?
        @tasksessions = @task.usersessions.sort_by { |p| p.student.lastname }
      else
        @tasksessions << @task.usersessions.find_by(associatedstudentsid: current_user.sid)
      end
    end
        
    if params[:show_comments_for]
      @show_comments_rec = Sessionrecording.find(params[:show_comments_for])
    else
      @show_comments_rec = false
    end
  end
      
  def load_comments_pane
    @task = Grouptask.includes(:usersessions, :sessionrecordings, :students).find(params[:id])
    @rec = Sessionrecording.includes(:comments).find(params[:rec])
    @parent_comments = @rec.comments.viewable_by([current_user,@task]).where("parent_id IS NULL")
    respond_to do |format|
     format.js  {}
    end
  end
  
  def play_at_timecode
    @comment = Comment.find(params[:id])
    timecode = params[:timecode]
    @seconds = (timecode.split(':')[0].to_i*60)+(timecode.split(':')[1].to_i)
    
    respond_to do |format|
     format.js  {}
    end
  end
  
  def insert_timecode    
    respond_to do |format|
     format.js  {}
    end
  end
  
  def reply_to_comment
    @parent = Comment.includes(:sessionrecording, :student).find(params[:parent])
    @recording = @parent.sessionrecording
    @task = @parent.grouptask
    respond_to do |format|
     format.js {}
    end
  end
    
  def create_comment
    @comment = Comment.create(comment_params(params))
    
    respond_to do |format|
      if @comment.save
        format.js {}
      else
        # flash[:notice] = "Comment was not saved!"
        format.js {}
      end
    end
  end
  
  def create_comment_reply
    @comment = Comment.create(comment_params(params))
    
    respond_to do |format|
      if @comment.save
        format.js {}
      else
        # flash[:notice] = "Comment was not saved!"
        format.js {}
      end
    end
  end
  
  def edit_comment
    @comment = Comment.find(params[:id])
    respond_to do |format|
     format.js {}
    end
  end
  
  def update_comment
    @comment = Comment.find(params[:id])

    respond_to do |format|
      if @comment.update(comment_params(params))
        format.js {}
      else
        # flash[:notice] = "Comment was not updated!"
        format.js {}
      end
    end
  end
  
  def cancel_comment
    @dom_id = params[:dom_id]
    
    respond_to do |format|
     format.js {}
    end
  end
  
  def cancel_comment_edit
    @dom_id = params[:dom_id]
    @comment = Comment.find(params[:id])
    
    respond_to do |format|
     format.js {}
    end
  end
    
  def destroy_comment
    @comment = Comment.find(params[:id])
    
    respond_to do |format|
      if @comment.destroy
        format.js {}
      else
        #flash[:notice] = "Comment was not deleted!"
        format.js {}
      end
    end
  end
  
  private
  
  def comment_params(params)
    params.require(:comment).permit(:body, :student_id, :kind, :sessionrecording_id, :parent_id)
  end
    
end