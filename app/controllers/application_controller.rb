class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  helper :all # include all helpers, all the time
  helper_method :current_user
  before_filter :setup_session_key
    
  private
  
    def protect
      unless session[:user_id]
        logger.info "Unauthenticated user redirected to login."
        flash[:notice] = "Please log in first!!!"
        redirect_away url_for(:controller => 'logins')
        return false
      end
    end
  
    def setup_session_key
      # Pick a unique cookie name to distinguish our session data from others'
      request.session_options[:session_key] = '_cornichon_session_id'
    end
  
    def current_user
      if session[:user_id]
        return Student.where(shortname: session[:user_id].downcase).first
      else
        return false
      end
    end
    
    # redirect somewhere that will eventually return back to here
    def redirect_away(*params)
      session[:original_uri] = request.url
      redirect_to(*params)
    end

    # returns the person to either the original url from a redirect_away or to a default url
    def redirect_back(*params)
      uri = session[:original_uri]
      session[:original_uri] = nil
      if uri
        redirect_to uri
      else
        redirect_to(*params)
      end
    end
  
end

DILL_WEB_FULL_URL = "#{SysParam.get(:DiLLWebServerHostURL)}cgi-bin/WebObjects/DiLL.woa"
USER_EMAIL_DOMAIN = "umich.edu"