module StatisticsHelper

  def users_with_frequency(cutoff)
    begin
      num_of_users = Student.use_frequency([@days_ago,cutoff]).count
      return "#{num_of_users} (#{((num_of_users/@num_of_total_profs.to_f)*100).round}%)"
    rescue
      return "No results"
    end
  end
  
  def sync_vs_async
    begin
      return "#{@sync_tasks} (#{((@sync_tasks.to_f/@num_of_total_tasks.to_f)*100).round}%) / #{@async_tasks} (#{((@async_tasks.to_f/@num_of_total_tasks.to_f)*100).round}%)"
    rescue
      return "No results"
    end
  end
  
  def lesson_vs_non
    begin
      return "#{@lesson_tasks} (#{((@lesson_tasks.to_f/@num_of_total_tasks.to_f)*100).round}%) / #{@nonlesson_tasks} (#{((@nonlesson_tasks.to_f/@num_of_total_tasks.to_f)*100).round}%)"
    rescue
      return "No results"
    end
  end
  
  def user_tasks_totals
    thetext = "<table id=\"user-task-totals\"><tr><th>Instructor Name</th><th># Tasks</th></tr>"
    sorted = Array.new
    for prof in @profs.each do
      taskcount = Grouptask.created_days_ago(@days_ago).where(creatorstudentid: prof.sid).count(:all)
      sorted << [prof, taskcount]
    end
    sorted.sort! {|x, y| y[1] <=> x[1]}
    for p in sorted.each do
      thetext << "<tr><td>#{p[0].firstname} #{p[0].lastname}</td><td>#{p[1]}</td></tr>"
    end
    thetext << "<tr><th>Total</th><th>#{@num_of_total_tasks}</th></tr>"
    thetext << "</table>"
    return thetext.html_safe
  end
  
end
