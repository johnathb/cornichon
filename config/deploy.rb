# config valid only for current version of Capistrano
# lock '3.4.0'

set :application, 'cornichon'
set :repo_url, 'git@bitbucket.org:johnathb/cornichon.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/home/cornilrc/cornichon'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
set :pty, false

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml')
set :linked_files, fetch(:linked_files, []).push('public/.htaccess')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('bin', 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 3

set :branch, "master"
set :use_sudo, false

set :rails_env, "production"

set :deploy_via, :remote_cache
set :copy_exclude, [ '.git' ]
set :keep_releases, 5

server "cornichon.lrc.lsa.umich.edu", user: 'cornilrc', roles: [:app, :web, :db], :primary => true

set :linked_dirs, fetch(:linked_dirs) + %w{public/system public/uploads}

set :ping_url, "https://cornichon.lrc.lsa.umich.edu/"

set :passenger_restart_with_touch, true

set :rvm_ruby_version, 'ruby-2.4.1'
set :passenger_rvm_ruby_version, 'ruby-2.4.1'

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end
  
  desc 'Warm up the application by pinging it, so enduser wont have to wait'
  task :ping do
    on roles(:app), in: :sequence, wait: 5 do
      execute "curl -s -D - #{fetch(:ping_url)} -o /dev/null"
    end
  end
 
  after :restart, :ping

end